An auto-completing search component for GitHub users using GitHub API

What do I like about my solution
- It's straightforward and the core functionality is not dependent on any existing library
- I added the hover functionality because I think it's very common with search result list
- The style is consistent with the overall style of GitHub

What do you dislike about your solution?
- Didn't have time to proper document the code
- Didn't solve the problem of 403 forbidden for the request and
  didn't figure out how to use the access token in code
- There is a bit of redundancy in the code, can possibly be improved

If you had a full day more to work on this, what would you improve?
- The access token thing
- Would love to get some feedback from senior first and then improve code efficiency

If you would start from scratch now, what would you do differently?
- It's just a couple of hours so I don't think the result would be very different,
  some advice from senior developers will help
- I spent a lot of time fixing Atom (my Webstorm expired because I am not student anymore),
  might as well just purchase a yearly license of Webstorm because coding and debugging is
  much easier with Websotrm >_<
