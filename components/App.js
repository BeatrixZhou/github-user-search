import React, {Component} from 'react';
import {Header} from './menu/Header';
import AutocompleteSearch from './body/AutocompleteSearch';

export default class App extends Component {
  render () {
    return (
      <div>
      <Header />
      <AutocompleteSearch />
      </div>
    );
  }
}
