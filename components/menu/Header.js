import React from 'react';
import '../../styles/header.css';

export const Header = (props) => (
  <div>
      <img src="../../resources/images/GitHub-Mark-32px.png" />
      <h1>Search in GitHub</h1>
  </div>
);
