import React, {Component} from 'react';
import ArrowKeysReact from 'arrow-keys-react';
import {List} from './List';
import '../../styles/body.css';

const url="https://api.github.com/search/users?q=";
const maxPage = 10;
const userProfileUrl = "https://github.com/";

export default class AutocompleteSearch extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      showResults: false,
      users: [],
      highlightIndex: -1
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.updatehighlightIndex = this.updatehighlightIndex.bind(this);

    ArrowKeysReact.config({
      up: () => {
        if (this.state.highlightIndex > 0) {
          this.setState((prevState, props) => ({
            highlightIndex: prevState.highlightIndex - 1,
          }));
        }
      },
      down: () => {
        if (this.state.highlightIndex < maxPage - 1) {
          this.setState((prevState, props) => ({
            highlightIndex: prevState.highlightIndex + 1,
          }));
        }
      }
    });
  }

  handleChange(event) {
    if (event.target.value === "") {
      this.setState(() => ({
        showResults: false,
        highlightIndex: -1,
        users: []
      }));
      return;
    } else {
      fetch(url + event.target.value)
      .then(function(response) {
        return response.json();
      })
      .then(function(data) {
        this.setState(() => ({
          users: data.items,
        }));
      }.bind(this))
      .catch(function(err) {
        this.setState(() => ({
          showResults: false,
          highlightIndex: -1,
          users: []
        }));
      }.bind(this));
      if (this.state.users && this.state.highlightIndex === -1) {
        this.setState(() => ({
          highlightIndex: 0,
          showResults: true
        }));
      }
    }
  }

  handleKeyPress(event) {
    if (event.key === "Enter") {
      window.open(userProfileUrl + this.state.users[this.state.highlightIndex].login);
    }
  }

  updatehighlightIndex(index) {
    this.setState(() => ({
      highlightIndex: index
    }));
  }

  render() {
    return (
      <div>
        <div {...ArrowKeysReact.events} tabIndex="1" className="cardBg">
          <div className="searchBoxContainer">
            <input autoFocus className="centerHorizontal" type="text"
              onChange={this.handleChange}
              onKeyPress={this.handleKeyPress} />
          </div>
          <div className="resultContainer">
            {this.state.showResults
              ? <List users={this.state.users} highlight={this.state.highlightIndex}
                  updateHighlight={this.updatehighlightIndex}/>
              : null}
          </div>
        </div>

        <div className="cardBg placeholder">
          <p>placeholder</p>
        </div>
      </div>
    )
  }

}
