import React from 'react';

const userProfileUrl = "https://github.com/";
const maxPage = 10;

export function List(props) {

  var handleHover = function(index) {
    props.updateHighlight(index);
  }

  var handleClick = function(index) {
    window.open(userProfileUrl + props.users[index].login);
  }

  return (
    props.users
    ? <div className="result">{props.users.map((obj, i) => {
        return (
          <div key={i} index={i}>
            {i < maxPage
              ? <div className="clickable resultRow"
                  onMouseOver={handleHover.bind(this, i)}
                  onClick={handleClick.bind(this, i)}
                  style={{backgroundColor: props.highlight === i ? "#f6f8fa" : "#ffffff"}}>
                <img className="inline avatar"
                  src={props.users[i].avatar_url} width="32px" height="32px" />
                <div className="inline">{obj.login}</div>
              </div>
              : null
            }
          </div>
        );
      })
    }</div>
    : null
  )
}
