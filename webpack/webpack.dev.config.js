const path = require('path');
var parentDir = path.join(__dirname, '../');
var webpack = require('webpack');

module.exports = {
	entry: [
		path.join(parentDir, 'index.js')
  ],
	devtool: '#source-map',
	output: {
	  path: parentDir + '/dist',
	  filename: 'bundle.js'
	},
  module: {
    rules: [
			{
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [{
          loader: "babel-loader"
        }]
      },
		  {
        test: /\.css$/,
        loader: ["style-loader", "css-loader"]
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.ico|\.svg$|\.woff$|\.ttf$/,
        loader: "file-loader"
      },
	  ]
  },
	devServer: {
    contentBase: parentDir,
		historyApiFallback: true
  },
	plugins: [
     new webpack.LoaderOptionsPlugin({
       debug: true
     })
   ]
};
